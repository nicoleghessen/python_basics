# Define the following variables
# name, last_name, age, eye_color, hair_color, good_person=

first_name = "Nicole"
last_name = "Ghessen"
eye_color = "Green eyes"
hair_color = "Brown hair"
age = "23"
good_person = "Yes"

# Ask/Prompt user for input and Re-assign the above variables
print(first_name)
print(last_name)
print(eye_color)
print(hair_color)
print(age)
print(good_person)


# Print them back to the user as conversation
# Example: 'Hi there Jack! Your age is 36, you have green eye, black hair and your are a good person!"
print("Hi there Nicole, your age is 23 and you have Green eyes and Brown hair. You're a good person")


#Extra Question 2 - Year of birth calculation? and responde back.
# print something like: 'You told the machine you are 28 hence you must have been born 1991!.. give or take'


# Extra Question 3 - Cast your input so age is stored as numerical type