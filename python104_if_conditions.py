# if conditions
# if conditions are part of control flow.
## when a condition becomes True it runs a block of code.
## There is also an option to program for an an 'else' situation where no condition has become true.

# the syntax


from urllib.parse import _ParseResultBase


if True:
    print('it is true')
else:
    print('I will not be printed')

#New example for the else
if False:
        print('I will not be printed')
else:
        print('printing from the else')


#if <condition>:
    #    <block of code>
#elif <condition>:
      #  <block of code>
#else:
       # <block of code>

# python uses indentation to outline blocks of code.
# meaning: after the colon, a block of code starts where its indented and ends where the indentation ends.


# example - made into a comment after using so that I could reduce clutter in my terminal when practicing.

# weather= "sunny"
# weather= input('what is the weather   ').strip().lower()

# print(f"{weather}!")

# usually you want to treat user input and format for matching

# if weather == "sunny":
#     print("take shades")
#     print(f'the weather is {weather}')
# elif weather == "windy":
#     print('weather is windy')
#     print ('fly a kite')
# else:
#     print('weather is not nice')
#     print('take a coat')

## Matching with 'in'
# python has a "in" matcher that allows you to match with less restriction
# works in lists, numbers and strings

lottery_number = 124
your_numbers = [10, 120, 30, 50, 140, 124]

print(124 in your_numbers)
print(lottery_number in your_numbers)


magic_word = "please"

print(magic_word in "where are the cookies?")

user_question = input("> what would you like?")
if magic_word in user_question:
    print("you shall have what you wish")
else: print("you did not use the magic word...")

