## print command 
## print command makes code output to the terminal. Super useful for debugging!

print('hello world')

## ^ this does not mean that code cannot run without outputting to the terminal 

10*100
20*33
print (20*2) # the only one that will be output as we wrote print in front of it, the other two still happen just in the background.



## type ()
# help you identify the type of data, specifically what class? For example:
print(10)
print (type(10))
print('10')

# helps you figure out what that thing is- being a plumber of code and need to know what you're plugging with what.


## variables
# A variable is like a box, it has a name and we can put stuff inside and then get the stuff that's inside. 
# We can also at any point put other stuff inside the same box.
# we could have a box called books and put "Rich dad Poor dad" inside.

# assignment of variable books to a string
books = "Rich dad, poor dad"

# calling of the variable 
print(books)
print(type(books))
# that checks the type!

## prompting user for input 
# syntax input('message')

input('Tell me a number' )

# you need to capture the input into a variable to use later
user_var =input('Tell me a number' )

print(user_var)


## Python errors
### they always point you to the line that is broken + give you the type of error.


# syntax error
# no name error
# wrong data type