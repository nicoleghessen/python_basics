## Dictionaries in Python
# allow us to keep complex data. 
    # mike + his number and address
    # anna and her company
    # other people

# it's very simple, it works like a real dictionary.
# it jumps to the words you are looking for and has a value for it.

# syntax
# {}
# {"key" : value}
print(type({}))

example_dict = {
    "human1" : "Mike",
    "human2" : "Anna",
}

# print the dictionary
print(example_dict)

# use the keys to get values- like a list but with keys
print(example_dict["human2"])

# reassigning values
example_dict["human2"] = "Arnold"
print(example_dict)

# make key value pairs on the fly 
example_dict["human47"] = "secret agent 047"
print(example_dict)


###

person_1 = {
    "name" : "Britney",
    "age" : 30,
    "skills" : "Media"
}
print(person_1)


person_2 = {
    "name" : "Justin Timberlake",
    "age" : 35,
    "skills" : "Other"
}

