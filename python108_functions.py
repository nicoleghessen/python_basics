# Functions 
# functions are like a machine, they can take in arguments, do some work and output a value.
# to work, functions must be called


# Concepts:
# DRY- Don't Repeat Yourself
    # avoid repetition in your code - functions can help
    # maintainable 

# Good functions:
    # Are like people- they should have 1 job!
    # makes it easier to manage
    # makes it easier to measure
    # and test
    # IMPORTANT: don't print inside a function. you return

# syntax
# def <function_name>(arg1, arg2, arg*):
    # block_code
    # block_code
    # return <value>

# defining function
# def say_hello():
#     return "helloooo"

# calling functions

# say_hello()
# print(say_hello())

# # function with arf
# def say_hello_human(human):
#     return "helloooo" + human
# print(say_hello_human('Nicoleeee'))

# def full_name_user_ask():
#     f_name = input('> provide a first name: \n>')
#     l_name = input('> provide a last name: \n>')

#     return f_name + ' ' +l_name

# print(full_name_user_ask())

# you can have defaults

def full_name(f_name="Alan", l_name="Turin"):
    return  f_name + ' ' + l_name
print(full_name('Nicole'))
print(full_name(l_name='Ghessen'))
