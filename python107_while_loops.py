# While loops
# a loop with a condition 
# Don't make endless loops! Like dividing by zero



# syntax - break condition at top
# while <condition>:
    # block of code
    # block of code


# syntax - break condition in body
# while True:
    # block of code
    # block of code
    # <condition>:
        # break

# counter = 0
# while counter < 10:
#         print("I'm in the while looooppp")
#         print(counter)
#         counter += 1



# counter = 0
# while True:
#     print("Im in the while looppp")
#     if counter > 10:
#         break
#     counter += 1

### Create small whitch boards- good for small games/apps

while True:
    print("welcome to the matrix")
    print("Press 1 for hello")
    print("Press 2 for surprise")
    print("Press 3 for goodbye")
    user_input = int(input("Please choose an option").strip())


# use user input to control flow what actions
    if user_input == 1:
        print("howdy partner")
    elif user_input == 2:
        print("surprise")
    elif user_input == 3:
        print("goodbye")
        break
    else:
        print("you must choose between 1-3")