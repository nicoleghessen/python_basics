# Python 101

Python is a programming language that is syntax light and easy to pick up. It is very versatile and well paid.

Used across the industry for:
- simple scripts
- automation (ansible)
- web development 
- data and ml
- others


List of things to cover:
- Data types
- Strings
- Numericals 
- Booleans
- Lists
- Dictionaries
- If conditions 
- Loops
- Debugger
- While loops
- Functions
- TDD and Unit testing
- Error handling 
- External packages 
- API's


Other topics to talk about:
- TDD
- Polymorphism (when something can take many forms)
- DRY code
- Seperation of concerns 
- ETL- extract transform and load
- Frameworks (difference to actual langauges)


### Python Introduction and basics

Python you can make files and run them,you can run on the command line or you can have a Framework running python or use an IDE.

To try stuff out super quick, use the command line.
```bash
python3 
>>>>
```

Then you can write python.

```python
human = "Nicole Ghessen"
>>> print (human)
Nicole Ghessen
```

## virtual environment 

.venv folder is a virtual enviornment for python, where you can install packages with pop locally per project, rather than globally in your machine. 

You need to start a venv enviornment and activate it, using 'source'.

code

```bash

python3 -m venv .venv
source .venv/bin/activate

```

This is to avoid package collision on your machine and contains the python packages installed to this folder.

**requirements.txt** Is a text file where you can specify a list of packages your code will need. You can then load it / read it using pup. It also locks in versions.

Read it with pip or pip3 using this code:

```bash
pip install -r requirements.txt
```
This will install all the packages in requirements.txt.