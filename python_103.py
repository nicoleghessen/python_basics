## Lists

# a list is a numerically organised list of objects, lists are organised using indexes that start at 0.
# a list can be anything, from a shopping list or a list of crazy landlords/employers.

# syntax []
print(type([]))

# you can define a list with several items 
my_crazy_employer = ["Walt Disney", "Looney Toons", "Carton Network"]

# call the list/print everything
print(my_crazy_employer)

# lists are organised with index- starting at 0
# (listen to recording for explanation)
# list_name   = [ 0   ,   1   ,   2   ,    3]


# use index to get individual items from the list
print(my_crazy_employer[0])
print(my_crazy_employer[1])
print(my_crazy_employer[-1])


## add something to the list
# .append()
my_crazy_employer.append('channel 4')
print(len(my_crazy_employer))


## remove using index and delete
my_crazy_employer.pop(1)
print(my_crazy_employer)

## lists are mutable and can take multiple data types

new_list = [10, 11, 20, "hello", "multiple data", [10, 20,"yeah"]]

print(new_list)

print(type(new_list[0]))
print(type(new_list[3]))

## lists are mutable and you can reassign an index position
new_list[1] = 200000
print(new_list[1])

