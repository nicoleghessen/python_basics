# For loops

# syntax
# for place holder in <iterable>:
    # block_of_code


farhiaya_gifts = ['Roller Blades', 'Roller Skates', 'Vacay']

for item_placeholder in farhiaya_gifts:
    print(item_placeholder)


## embedded lists
farhiaya_gifts_2 = ['Roller Blades', 'Roller Skates', 'Vacay'], ['stuffed toys', 'teddy bear', 'bike']

for list_item in farhiaya_gifts_2:
    print(list_item)



## Iterating over a dictionary

person_2 = {
"name" : "Justin Timberlake",
"Age" : 34,
"Skills" : "Other"
}

for key in person_2:
    print(key)
    # how do I get the values? :) dictorinary + key
  #  print(person_2[key])

# for key in person_2.keys()
# print(person_2[key])