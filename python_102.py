# Strings
# a list of unorganised characters that can have spaces, numbers and any characters.
# syntax "" or ''
print('this is a string')
print(type("this too is a string"))

my_string = "this is a nice simple string"
# len() -> length of string
print(len(my_string))


# useful string methods 
# object.capitalize() --> makes the first character upper case

print(my_string.capitalize())

# .lower() --> all characters lowercase
print(my_string.lower())

# .upper() --> makes it all uppercase
print(my_string.upper())

# .title() --> first letter of every word capitalised
print(my_string.title())


# strings can be concactenated
## the joining of two strings 

name = "Nicole"
greeting = 'Hey there'

print(greeting + ' ' +name)
print(greeting, name)


# interpolation with f strings
# you can interpolate into a string a value.

# syntax print (f""")
print("Welcome and howdy <name>!")
print(f"Welcome and howdy {name}!")

# code is written in {} brackets 
print(f"Welcome and howdy {name.upper()}!")


# String.. as said in the start- are lists of characters, but they are lists.
print(name[0])
# this will output the first letter of the name variable.


# Numerical types
# there are numbers, we have integers, flats, and a few others that are less used.
# you can perform mathmatical arithatic with them.

my_number = 16
print (my_number)
print (type(my_number))

num_a = 8
num_b = 24

# you can add, subtract, divide, multiply and others.
print(num_a + num_b)
print(num_a - num_b)
print(num_b * 60005)

# all of the above are intergers
## just a whole number

# Floats are composite numbers, everytime you divide you also get a float.
print(type(3.14))

## there are comparison operators that you can use logically with maths.
## such as greater than, smaller than, and so on.
# the result is what is called a boolean.

# A boolean is a data type that is either true or false.
# this helps build logic in computers as it's one or the other.

# Booleans- True or False
print (type(True)) #<class 'bool'>
print(type(False)) #<class 'bool'>

## Comparison operators:
num_a = 8
num_b = 24

#greater than
print(num_a > num_b) # so this would be false and the type would also be a boolean.

#smaller than
print(num_a < num_b)

# greater than or equal
print(num_a >=8)
print(num_a >= num_b)

# smaller than or equal 
print(num_a <=8)
print(num_a <= num_b)

# equal to
print(num_a == num_b) # double equal so it doesn't assign it, hence most languages use == or ===(if js) so it is broken.

# not equal to 
print(num_a !=num_b)

## comparison operator also works for strings

print('isthe string 10 equal to integer 10 in py?', '10' == 10)

example_input= 10

user_input=input("> provide input:  ")

print(example_input == user_input)


#Casting
## user input is captured as a string 
## helps us change data type where possible 
## very useful when taking in user input.

## change a integer to a string
# str()

example_int= 101
example_str= str(example_int)
print (type(example_str))

## change str into an integer
## will only work with clean strings using numerical types only 
print(type(int('10')))
